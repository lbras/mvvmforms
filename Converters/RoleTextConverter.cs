﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Forms;
using MvvmForms.Models;

namespace MvvmForms.Converters
{
    /// <summary>
    /// Converts a user role to a textual description and vice-versa.
    /// </summary>
    public class RoleTextConverter : IValueConverter
    {
        /// <summary>
        /// Converts a user role to a textual description.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((Role)value)
            {
                case Role.Admin:
                    return "Bazinga!";

                case Role.User:
                    return "Minion";

                default:
                case Role.Guest:
                    return "Milk and Wi-Fi thief";
            }
        }

        /// <summary>
        /// Converts a textual description to a user role.
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((string)value)
            {
                case "Bazinga!":
                    return Role.Admin;

                case "Minion":
                    return Role.User;

                default:
                case "Milk and Wi-Fi thief":
                    return Role.Guest;
            }
        }
    }
}
