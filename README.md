Demo application for how to use MVVM in Windows Forms
======================================================

This demo application shows how to use a simplified MVVM pattern in Windows Forms.

In Windows Forms there is no XAML to bind controls to the ViewModel properties, however the same can be done through the designer's property window. The goal here is to let the designer take care of all the plumbing necessary to setup data bindings between controls and ViewModel's properties. Of course, this is not nearly as powerful as XAML data bindings, but it effectively minimizes code in the Form's code-behind, leveraging most of the advantages of MVVM.

A BindingSource is used to represent a specific ViewModel. The controls' properties are bound to a ViewModel's properties, through the BindingSource. At runtime, the BindingSource's DataSource property is set to the actual ViewModel instance, in the Form's code-behing - similar to setting the DataContext in WPF.

ICommand support
--

ICommand is supported through custom controls that are aware of Commands. Included in the app is a CommandButton, ToolStripCommandButton, ToolStripCommandMenuItem and ToolStripCommandSplitButton. That should be enough for most uses, if not it is very easy to add support for other controls.

The CanExecuteChanged event has to be raised explicitly. Check the included RelayCommand, adapted from Josh Smith MVVM example.


IValueConverter support
---

Not really related to MVVM, but I also included an example of using a value converter in Windows Forms bindings, which will convert an enum property from the ViewModel to a string representation and back. This has to be done in code-behind, unfortunately, although it's only a one liner. An extension method adds the Bind method to any IValueConverter. This method  binds a control's property to a ViewModel's property using the IValueConverter.

The text of the Role textbox is a textual representation of the enum Role:

* Role.Admin: "Bazinga!"
* Role.User: "Minion"
* Role.Guest: "Milk and Wi-Fi thief"

To see the converter in action, change a user role from "Bazinga!" to "Minion" and you'll see the Admin checkbox state changing. I know, this is probably the worst IValueConverter ever written...


Binding the View to the ViewModel
-------------

Generally, once you have your Models and ViewModels ready and your View [Form] designed, follow this process to bind the View to the ViewModel:

1. Add a BindingSource for the ViewModel. Just drag it from the ToolBox to anywhere in your Form. It will show on the bottom of the designer window.

2. Right-click the BindingSource you just added and open its properties. Click the arrow on the DataSource property and select "Add project data source". Select Object and locate the ViewModel type you want to use.

3. Now you can bind controls to that ViewModel using the BindingSource. Select a control and open its properties window, as before.

4. Find the DataBindings group. If the control's property you want to bind is not in that list, click "Advanced" and you'll see all the properties that can be bound.

5. Either way, when you click the arrow you should see the BindingSource you added. If you expand it, the properties of the ViewModel become visible. Select one and you're set. You might want to change the data source update mode in the Advanced window.