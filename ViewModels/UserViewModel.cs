﻿using MvvmForms.Models;

namespace MvvmForms.ViewModels
{
    /// <summary>
    /// ViewModel for the user.
    /// </summary>
    class UserViewModel : ViewModelBase
    {
        private readonly User _user;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel" /> class.
        /// </summary>
        /// <param name="user">The user.</param>
        public UserViewModel(User user)
        {
            _user = user;
        }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        /// <value>
        /// The user name.
        /// </value>
        public string Name
        {
            get { return _user.Name; }
            set
            {
                if (_user.Name == value)
                    return;
                _user.Name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this user is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this user is active; otherwise, <c>false</c>.
        /// </value>
        public bool IsActive
        {
            get { return _user.IsActive; }
            set
            {
                if (_user.IsActive == value)
                    return;
                _user.IsActive = value;
                OnPropertyChanged("IsActive");
            }
        }

        /// <summary>
        /// Gets or sets the user role.
        /// </summary>
        /// <value>
        /// The user role.
        /// </value>
        public Role Role 
        {
            get { return _user.Role; }
            set
            {
                if (_user.Role == value)
                    return;
                _user.Role = value;
                OnPropertyChanged("Role");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this user is admin.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this user is admin; otherwise, <c>false</c>.
        /// </value>
        public bool IsAdmin
        {
            get { return _user.Role == Role.Admin; }
            set
            {
                if (value && _user.Role == Role.Admin || !value && _user.Role != Role.Admin)
                    return;
                _user.Role = value ? Role.Admin : Role.User;
                OnPropertyChanged("IsAdmin");
            }
        }
        
    }
}
