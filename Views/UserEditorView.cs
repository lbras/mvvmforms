﻿using System.Windows.Forms;
using MvvmForms.ViewModels;
using MvvmForms.Converters;

namespace MvvmForms.Views
{
    /// <summary>
    /// A view bound to ViewModels.
    /// </summary>
    public partial class UserEditorView : Form
    {
        public UserEditorView()
        {
            InitializeComponent();
            _bsrcUserEditorViewModel.DataSource = new UserEditorViewModel();

            // To use a converter, the binding has to be created programmatically
            new RoleTextConverter().Bind(_txbRole, "Text", _bsrcUserViewModels, "Role");
        }
    }
}
