﻿using System.Collections.Generic;

namespace MvvmForms.Models
{
    /// <summary>
    /// User model.
    /// </summary>
    public class User
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="User" /> class, using
        /// the specified <paramref name="name"/> and <paramref name="role"/>.
        /// </summary>
        /// <param name="name">The user name.</param>
        /// <param name="role">[Optional] The user role. Defaults to Role.User."/></param>
        public User(string name, Role role = Role.User)
        {
            Name = name;
            Role = role;
            IsActive = true;
        }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this user is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this user is active; otherwise, <c>false</c>.
        /// </value>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the user role.
        /// </summary>
        /// <value>
        /// The user role.
        /// </value>
        public Role Role { get; set; }

        /// <summary>
        /// Gets all existing users from the data repository.
        /// </summary>
        public static IEnumerable<User> GetUsers()
        {
            yield return new User("Sheldon", Role.Admin);
            yield return new User("Leonard");
            yield return new User("Howard");
            yield return new User("Rajesh");
            yield return new User("Penny", Role.Guest);
        }
    }

    /// <summary>
    /// Represents a user role.
    /// </summary>
    public enum Role
    {
        Guest,
        User,
        Admin
    }
}
